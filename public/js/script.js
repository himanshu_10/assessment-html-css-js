let prodList = []
let wishLIst = []

// getting the data
function getProducts(){
   return fetch("http://localhost:3000/products",{method:"GET"})
   .then((res)=>{
      if(res.ok){
        return res.json()
      }
      else{
        return Promise.reject(res.status);
      }
   })
   .then((data)=>{
      prodList = data;
      displayProducts(prodList);
      return data;
   })
   .catch((err)=>{
    console.log(err)
   })
}



//This fn will fetch save for later from db.json

function getSaveForLater(){
    return fetch("http://localhost:3000/saveforLater",{method:"GET"})
    .then((res)=>{
        if(res.ok){
          return res.json();
        }
        else{
          return Promise.reject(res.status);
        }
     })
     .then((data)=>{
        wishLIst = data;
        displayWishList(wishLIst);
        return data;
     })
   .catch((err)=>{
    console.log(err)
   })
}


//This fn will display products on html page

let displayProducts = (productArr) =>{
    let input = ""
    productArr.map(element => {
        input += `
                       <div class="prod-card col-sm">
                       <li><img src='${element.thumbnail}'></li>
                       <div class='card-body'>
                       <li><h2 class='card-title'>${element.title}</h2></li>
                       <li><div>${element.description}</div></li>
                       <li><div>PRICE: ${element.price}</div></li>
                       <li><div>RATING: ${element.rating}</div></li>
                       <li><div>STOCK: ${element.stock}</div></li>
                       <li><div>CATEGORY: ${element.category}</div></li>
                       <li><button class="btn btn-success" onclick="addToWishList(${element.id})">Add to WishList</button></li>
                       </div>
                       </div>
                      `
    })
    document.getElementById("prod_List").innerHTML = input;
}

//This fn will display saved items on html page


let displayWishList = (saveLaterArr) =>{
    let productId = document.getElementById("wish_List");
    let htmlString = ""
    saveLaterArr.forEach(element => {
        htmlString +=
        `
        <div class="prod-card col-sm">
        <li><img src='${element.thumbnail}' class='card-img-top'></li>
        <div class='card-body'>
        <li><h2 class='card-title'>${element.title}</h2></li>
        <li><div>${element.description}</div></li>
        <li><div>PRICE: ${element.price}</div></li>
        <li><div>RATING: ${element.rating}</div></li>
        <li><div>STOCK: ${element.stock}</div></li>
        <li><div>CATEGORY: ${element.category}</div></li>
        <li><button class="btn btn-danger" onclick="deleteItem(${element.id})">Delete Item</button></li>
        </div>
        </div>
        `

    })
    productId.innerHTML = htmlString;
}


//add items to wish list

function addToWishList(id){
   let product = prodList.find(item=>{
     if(item.id == id){
        return item;
     }
   })


   let savedItem = wishLIst.find(item=>{
      if(item.id == id){
        return item;
      }
   })
   if(savedItem){
     alert("This item is already in saveForLater")
     return Promise.reject(new Error("Item is already in saveForLater"));
   }
   else{
     return fetch('http://localhost:3000/saveforLater',{
        method : 'POST',
        headers : {'content-type':'application/json'},
        body: JSON.stringify(product)
     })
     .then((res)=>{
        if(res.ok){
            return res.json()
        }
     })
     .then((data)=>{
        wishLIst.push(data);
        displayWishList(wishLIst);
        return data;
     })
   }
}


//This fn will delete items from saveforLater

function deleteItem(id){
        return fetch(`http://localhost:3000/saveforLater/${id}`,{method:'DELETE'})
        .then((res) => {
            if(res.ok){
                return res.json()
            }
        })
        .then((data) => {
            wishLIst.splice(id);
            displayWishList(wishLIst);
            return data;
        })
    }


getProducts();
getSaveForLater();
